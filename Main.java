package ru.unn.project1;
import java.util.Scanner;
public class Main {
    public static void main(String[] args){
    Scanner in = new Scanner (System.in);
    System.out.println("Введите знак :");
    String znak = in.nextLine();
    System.out.println("Введите числа :");
    double num1 = in.nextDouble();
    double num2 = in.nextDouble();
    double otvet = 0;
    switch (znak) {
        case "+":
            otvet = num1 + num2;
            break;
        case "-":
            otvet = num1 - num2;
            break;
        case "*":
            otvet = num1 * num2;
            break;
        case "/":
            otvet = num1 / num2;
            break;
        default:
            System.out.println("Такой операции нет");
    }
        System.out.println("Ответ:" + otvet);
    }
}

